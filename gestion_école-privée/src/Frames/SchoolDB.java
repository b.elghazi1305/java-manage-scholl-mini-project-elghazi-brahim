
package Frames;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SchoolDB {
    public Connection con;
    public Statement stm;
    public ResultSet rst; 
    public   SchoolDB(){
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/manage_school","root","");
            stm=con.createStatement();

            }catch(ClassNotFoundException | SQLException e){
                System.out.println("Erreur de chargement de pilote:"+e);
            }
            
    }
    public void create_tables(){
        try{ 
//            ---------------------Etudiant Table---------------------------
            String query = "CREATE TABLE `etudiant` (" +
                        "`id` int Primary key AUTO_INCREMENT," +
                        "`nom` varchar(30)," +
                        "`prenom` varchar(30)," +
                        "`age` int," +
                        "`sexe` char(1)," +
                        "`department_id` int," +
                        "`password` varchar(30)" +
                        ");";
            stm.executeUpdate(query);
//             ---------------------Prof Table---------------------------
            query = "CREATE TABLE `Prof` (" +
                        "`id` int Primary key AUTO_INCREMENT," +
                        "`nom` varchar(30)," +
                        "`prenom` varchar(30)," +
                        "`age` int," +
                        "`sexe` char(1)," +
                        "`salaire` float," +
                        "`password` varchar(30)" +
                        ");";
            stm.executeUpdate(query);            
//            ---------------------Cour Table---------------------------
             query = "CREATE TABLE `Cour` (" +
                        "`Nom` varchar(30)," +
                        "`Note` int," +
                        "`Etud_id` int" +
                        ");";
            stm.executeUpdate(query);          
//            ---------------------Director Table---------------------------
             query = "CREATE TABLE `Director` (" +
                        "`nom` varchar(30)," +
                        "`prenom` varchar(30)," +
                        "`age` int," +
                        "`sexe` char(1)," +
                        "`password` varchar(30)" +
                        ");";
            stm.executeUpdate(query);

            }catch(SQLException e){
                System.out.println("Erreur : "+e.getMessage());
            }
        
    }
    public void add_etudiant(String prenom,String nom,int age,char sexe,int department_id,String password){
        try{
            String query = "INSERT INTO etudiant(prenom,nom,age,sexe,department_id,password)"
     + " VALUES ('" + prenom + "','" + nom + "'," + age + ",'" + sexe + "'," + department_id + ",'" + password + "')";
            stm.executeUpdate(query);
            }catch(SQLException e){
                System.out.println("Erreur de chargement de pilote:"+e);
            }
    
    }
    public void add_prof(String prenom,String nom,int age,char sexe,float salaire,String password){
        try{
           String query = "INSERT INTO prof(prenom,nom,age,sexe,salaire,password)"
            + " VALUES ('" + prenom + "','" + nom + "'," + age + ",'" + sexe + "','" + salaire + "','" + password + "')";
            stm.executeUpdate(query);                    
            }catch(SQLException e){
                System.out.println("Erreur de chargement de pilote:"+e);
            }
    
    }
     public void add_cour(String Nom,int Note,int Etud_id){
        try{
            String query = "INSERT INTO cour(Nom, Note, Etud_id)"
              + " VALUES ('"+Nom+"','"+Note+"','"+Etud_id+"')";
            stm.executeUpdate(query);
            }catch(SQLException e){
                System.out.println("Erreur de chargement de pilote:"+e);
            }
    
    }
    
    public ResultSet Etudient_data(){
        try{ 
           String query="select * from etudiant" ;
           rst=stm.executeQuery(query);
        
        }catch(SQLException e){
                System.out.println("Erreur : "+e.getMessage());
        }
        return rst;
    }
    public ResultSet director_data(){
        try{ 
           String query="select * from Director" ;
           rst=stm.executeQuery(query);
        
        }catch(SQLException e){
                System.out.println("Erreur : "+e.getMessage());
        }
        return rst;
    }
       public ResultSet prof_data(){
        try{ 
           String query = "SELECT * FROM prof";
           rst=stm.executeQuery(query);
        
        }catch(SQLException e){
                System.out.println("Erreur : "+e.getMessage());
        }
        return rst;
    }
    public ResultSet prof_data_byId(int id){
        try{ 
           String query = "SELECT * FROM prof WHERE id = " + id;
           rst=stm.executeQuery(query);
        
        }catch(SQLException e){
                System.out.println("Erreur : "+e.getMessage());
        }
        return rst;
    }
    public ResultSet cour_data(){
        try{ 
           String query="select * from cour" ;
           rst=stm.executeQuery(query);
        
        }catch(SQLException e){
                System.out.println("Erreur : "+e.getMessage());
        }
        return rst;
    } 
    public ResultSet Etudient_data_ById(int id){
        try{ 
            
           String query="select id,nom,prenom,age,sexe,department_id  from etudiant"
                   + " where id="+id ;
           rst=stm.executeQuery(query);
        
        }catch(SQLException e){
                System.out.println("Erreur : "+e.getMessage());
        }
        return rst;
    } 
    public ResultSet cour_data_ById(int id){
        try{ 
           String query="select * from cour where Etud_id="+id ;
           rst=stm.executeQuery(query);
        
        }catch(SQLException e){
                System.out.println("Erreur : "+e.getMessage());
        }
        return rst;
    } 
    public ResultSet Etudient_cour_data(){
        try{ 
           String query="select E.id,E.nom,E.prenom,E.age,E.sexe,E.department_id,C.Nom,C.Note "
                   + "from etudiant E,cour C where (E.id=C.Etud_id) order by E.id" ;
           rst=stm.executeQuery(query);
        
        }catch(SQLException e){
                System.out.println("Erreur : "+e.getMessage());
        }
        return rst;
    }
    public void Modifier_Cour(int id,int note_new,String nom){
       String query = "UPDATE cour "
              + "SET Note = " + note_new + " "
              + "WHERE Etud_id = " + id + " AND Nom = '" + nom + "'";
         try {
             stm.executeUpdate(query);
         } catch (SQLException ex) {
             Logger.getLogger(SchoolDB.class.getName()).log(Level.SEVERE, null, ex);
         }
        
    
    }
    public void delete_cour_ById(int id,String nom){
         String query="delete from cour WHERE Etud_id = " + id + " AND Nom = '" + nom + "'";
         try {
             stm.executeUpdate(query);
         } catch (SQLException ex) {
             Logger.getLogger(SchoolDB.class.getName()).log(Level.SEVERE, null, ex);
         }
    
    
    }
    public void delete_prof_ById(int id){
         String query="delete from prof WHERE id = " + id;
         try {
             stm.executeUpdate(query);
         } catch (SQLException ex) {
             Logger.getLogger(SchoolDB.class.getName()).log(Level.SEVERE, null, ex);
         }
    
    
    }
    
    public void delete_etud_ById(int id){
         String query="delete from etudiant WHERE id = " + id;
         String query2="delete from cour WHERE Etud_id  = " + id;
         try {
             stm.executeUpdate(query);
             stm.executeUpdate(query2);
         } catch (SQLException ex) {
             Logger.getLogger(SchoolDB.class.getName()).log(Level.SEVERE, null, ex);
         }
    
    
    }
    
    
}
